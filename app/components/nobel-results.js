const template = document.createElement('template');
template.innerHTML = `
<style>
    :host {
        position: relative;
        display: none;
        font-family: sans-serif;
        text-align: center;
    }
    :host(.initialised){
        display: block
    }

    h2 {
        border-bottom: 2px solid rgb(200,200,200);
        margin: 10px 4vh;
        padding: 10px 0;
    }

    .nobel-list {
        max-width: 1600px;
        padding: 2vh 2vw;
        margin: 0 auto;

        display: flex;
        flex-wrap: wrap;
        justify-content: space-evenly;
        align-items: flex-start;
        align-content: flex-start;
        box-sizing: border-box;
    }

    .nobel-list li{
        width: 95vw;
        padding: 15px 25px;
        margin: 10px 5px;
        box-sizing: border-box;

        border: 2px solid rgb(200, 200, 200);
        border-radius: 15px;
        list-style-type: none;
    }
    .nobel-list li img{
        float: left;
        width: 108px;
        height: 150px;
        margin-right: 15px;
    }

    .nobel-list li h3,
    .nobel-list li div{
        text-align: left;
    }
    .nobel-list li div span{
        display: inline-block;
        width: 4.5em;
        margin-right: 0.5em;
        text-align: right;
        font-weight: bold;
    }

    @media (min-width: 550px) {
        .nobel-list li{
            width: 450px;
        }
      }

</style>

<h2><slot>Search results</slot></h2>
<ul id="nobel-list" class="nobel-list">
</ul>

`;

/**
 * NobelResults component
 * Creating 'nobel-results' element
 * Presents Nobel Laureates search results in DOM
 * 
 * @author Rafal Maksymiuk <rafal.maksymiuk@gmail.com>
 * 
 */

class NobelResults extends HTMLElement {

    $nobelList;

    constructor() {
        super(); 
        this.attachShadow({ 'mode': 'open' });
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        this.$nobelList = this.shadowRoot.getElementById('nobel-list');
    }

    /**
     * Single list elemet constructor
     * @param {Object} data Single personal record from Nobel Laureates API
     */
    createListElement(data) {
        return `<li>
            <img src="http://nobel.divedeeper.io${data.photo_url.val[0]}" alt="${data.full_name.val[0]}"/>
            <h3 class="name">${data.full_name.val[0]}</h3>
            <div class="category"><span>category</span> ${data['Prize Category'].val[0]}</div>
            <div class="year"><span>year</span> ${data.year.val[0].act[1]}</div>
        </li>`;
    }
    
    /**
     * Empty list content
     */
    createEmptyListMsg(){
        return `<li><h3 class="name"> No results </h3></li>`
    }

    /**
     * New list data setter
     */
    set data(val) {
        let elementsString;
        if (val.length){
            elementsString = val.reduce( (prev, cur) => prev += this.createListElement(cur.result.fields), '');
        } else {
            elementsString = this.createEmptyListMsg();
        }
        this.$nobelList.innerHTML = elementsString;
        this.classList.add('initialised');
    }

}

window.customElements.define('nobel-results', NobelResults);