const template = document.createElement('template');
template.innerHTML = `
<style>

    /*loader borrowed from lading.io*/
    .lds-ellipsis {
        display: inline-block;
        margin: 0 0 -0 -80px;
        position: relative;
        width: 82px;
        height: 20px;
        opacity: 0;
        pointer-events: none;
        transition: opacity 400ms;
    }
    .lds-ellipsis div {
        position: absolute;
        top: 5px;
        width: 11px;
        height: 11px;
        border-radius: 50%;
        background: rgba(180, 180, 180, 0.5);
        animation-timing-function: cubic-bezier(0, 1, 1, 0);
    }
    .lds-ellipsis div:nth-child(1) {
        left: 6px;
        animation: lds-ellipsis1 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(2) {
        left: 6px;
        animation: lds-ellipsis2 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(3) {
        left: 26px;
        animation: lds-ellipsis2 0.6s infinite;
    }
    .lds-ellipsis div:nth-child(4) {
        left: 45px;
        animation: lds-ellipsis3 0.6s infinite;
    }
    @keyframes lds-ellipsis1 {
        0% {
        transform: scale(0);
        }
        100% {
        transform: scale(1);
        }
    }
    @keyframes lds-ellipsis3 {
        0% {
        transform: scale(1);
        }
        100% {
        transform: scale(0);
        }
    }
    @keyframes lds-ellipsis2 {
        0% {
        transform: translate(0, 0);
        }
        100% {
        transform: translate(19px, 0);
        }
    }


    :host {
        position: relative;
        display: block;
        font-family: sans-serif;
        text-align: center;
    }

    input{
        padding: 4px 0.4em;
        border: 2px solid rgb(180,180,180);
        border-radius: 10px;
        font-size: 2em;
        transition: color 200ms;
    }
    input::placeholder{
        color: rgb(210, 210, 210);
    }
    input:focus{
        outline: none;
    }

    input.searching{
        color: rgb(200, 200, 200);
    }
    .searching + .lds-ellipsis{
        opacity: 1;
    }

    .error {
        position: absolute;
        top: 23px;
        left: 50%;
        translate: -50% 0;
        font-size: 13px;
        color: red;
    }

</style>

<input type="text" placeholder="Search for laureate" id="search" ></input>
<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
<div class="error" id="error"></div>
`;

/**
 * NobelSearch component
 * Creating 'nobel-search' element
 * Allows for seaching within Nobel Laureates API with handy text input
 * 
 * @author Rafal Maksymiuk <rafal.maksymiuk@gmail.com>
 * @event searching triggers when component initiates the search
 * @event canceled triggers when component cancelled the search, most likelly due to error
 * @event result triggers when result is ready, details contain search results object
 * 
 */
class NobelSearch extends HTMLElement {

    /**
     * Should wildcard be used for search, default is 'false'
     */
    wildcard = false;

    /**
     * Debouncing delay, default is 500
     */
    delay = 500;

    $search;
    $error;

    /**
     * Container for previous request cancell function 
     */
    cancelPreviousFetch = () => {};

    /**
     * Events dictionary
     */
    static EVENTS = {
        SEARCHING: 'searching',
        CANCELED:  'canceled',
        RESULT:    'result',
    }

    static URL = 'http://nobel.divedeeper.io/twigkit/api/core/services/platform/search/platforms.workflow.laureates?q=';

    constructor() {
        super(); 
        this.attachShadow({ 'mode': 'open' });
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        this.$search = this.shadowRoot.getElementById('search');
        this.$error = this.shadowRoot.getElementById('error');

        this.$search.addEventListener('keyup', this.debounce(this.fetchData));
    }

    /**
     * Fetch Nobelists data
     */
    fetchData(){
        if (this.$search.value.length === 0){
            return;
        }

        this.cancelPreviousFetch();

        let success = (json) => {
            this.dispatchEvent(new CustomEvent(NobelSearch.EVENTS.RESULT, {detail: json.results}));
            this.$search.classList.remove('searching');
        }
        let failed = () => {
            this.dispatchEvent(new CustomEvent(NobelSearch.EVENTS.CANCELED));
            this.$search.classList.remove('searching');
            this.$error.textContent = 'Error fetching data.';
        }
        const controller = new AbortController();
        const signal = controller.signal;

        //debounce should do the trick, but in case second request initiates before previous ends
        this.cancelPreviousFetch = () => {
            //prevent executing any operations on prebvious request success or fail
            success = failed = () => {};
            controller.abort();
            this.$error.textContent = '';
        }

        this.dispatchEvent(new CustomEvent(NobelSearch.EVENTS.SEARCHING));
        this.$search.classList.add('searching');
        fetch(NobelSearch.URL + this.$search.value + (this.wildcard?'*':''), {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
            signal
        }).then( response => {
            if (!response.ok){
                return Promise.reject();
            }
            return response.json();
        }).then( (json) => {
            success(json);
        }).catch( () => {
            failed();
        })
    }

    /**
     * Debounce given callback
     * @param {Function} func 
     */
    debounce(func) {
        let timeout;
    
        return () => {
            clearTimeout(timeout);
            timeout = setTimeout(func.bind(this), this.delay);
        };
    }

    /**
     * Handle attributes changes
     * @param {String} name 
     */
    attributeChangedCallback(name) {
        switch(name){
            case 'wildcard':
                this.wildcard = this.hasAttribute('wildcard');
                break;
            case 'delay':
                this.delay = parseInt(this.getAttribute('delay'), 10);
                break;
        }
    }

    /**
     * Getters and setters
     */
    get wildcard() {
        return this.hasAttribute('wildcard');
    }
    set wildcard(val) {
        if (val) {
            this.setAttribute('wildcard', '');
        } else {
            this.removeAttribute('wildcard');
        }
    }
    get delay() {
        return parseInt(this.getAttribute('delay'), 10);
    }
    set delay(val) {
        if (val) {
            this.setAttribute('delay', delay);
        } else {
            this.removeAttribute('delay');
        }
    }

    /**
     * Configure observable attributes
     */
    static get observedAttributes() {
        return ['wildcard', 'delay'];
    }

}

window.customElements.define('nobel-search', NobelSearch);

export default NobelSearch;