import NobelSearch from './components/nobel-search.js';
import './components/nobel-results.js';

/**
 * Nobel Laureates Search application
 */
const $search = document.getElementById('search');
const $results = document.getElementById('results');

$search.addEventListener(NobelSearch.EVENTS.RESULT, (e) => {
    $results.data = e.detail;
    $results.classList.remove('searching');
});
$search.addEventListener(NobelSearch.EVENTS.SEARCHING, (e) => {
    $results.classList.add('searching');
});
$search.addEventListener(NobelSearch.EVENTS.SEARCHING, (e) => {
    $results.classList.add('searching');
});