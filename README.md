# Nobel Laureates API widgets.

## Technologies used:

All native ones. Native components implemented with template, custom elements and shadow dom. 
In addition native export & import.

## Requirements

Chrome 76 or never - should run in most moder browsers, although was not tested out of Chrome, and uses some cutting edge features as Static class properties that might only work in Chrome so far.

Node. There is NPM package.js config, but only needed for running http server. 
Page can be used statically anywhere, although not from local file system as any async requests will be broken.

Simply run:

```
npm install
npm start
```